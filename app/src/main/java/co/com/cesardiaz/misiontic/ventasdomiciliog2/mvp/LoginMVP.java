package co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.entity.User;

public interface LoginMVP {
    interface Model {
        void createUser(User user,
                        ValidateCredentialsCallback callback);

        void validateCredentials(String email, String password,
                                 ValidateCredentialsCallback callback);

        void getAllUsers(GetUserCallback<List<UserInfo>> callback);

        boolean hasAuthenticatedUser();

        Intent getGoogleIntent();

        void setGoogleData(Intent data, ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }

        interface GetUserCallback<T> {
            void onSuccess(T data);

            void onFailure();
        }
    }

    interface Presenter {
        void onLoginClick();

        void onFacebookClick();

        void onGoogleClick();

        void validateHasUserAuthenticated();

        void setGoogleData(Intent data);
    }

    interface View {
        Activity getActivity();

        LoginInfo getLoginInfo();

        void showEmailError(String error);

        void showPasswordError(String error);

        void showPaymentsActivity();

        void showGeneralError(String error);

        void showProgresBar();

        void hideProgresBar();

        void showGoogleSignInActivity(Intent intent);
    }

    class LoginInfo {
        private String email;
        private String password;

        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }

    class UserInfo {
        private String name;
        private String email;

        public UserInfo(String name, String email) {
            this.name = name;
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        @Override
        public String toString() {
            return "UserInfo {" +
                    "name='" + name + '\'' +
                    ", email='" + email + '\'' +
                    '}';
        }
    }
}
