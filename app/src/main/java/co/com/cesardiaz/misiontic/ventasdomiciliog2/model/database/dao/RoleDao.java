package co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.entity.Role;

@Dao
public interface RoleDao {

    @Query("SELECT * FROM role")
    List<Role> getAll();
}
