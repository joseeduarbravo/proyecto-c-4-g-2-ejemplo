package co.com.cesardiaz.misiontic.ventasdomiciliog2.presenter;

import android.os.Bundle;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.PaymentsInteractor;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp.PaymentsMVP;

public class PaymentsPresenter implements PaymentsMVP.Presenter {

    private PaymentsMVP.View view;
    private PaymentsMVP.Model model;

    public PaymentsPresenter(PaymentsMVP.View view) {
        this.view = view;
        this.model = new PaymentsInteractor(view.getActivity());
    }

    @Override
    public void loadPayments() {
        view.showProgressBar();
        new Thread(() -> model.loadPayments(new PaymentsMVP.Model.LoadPaymentsCallback() {
            @Override
            public void setPayments(List<PaymentsMVP.PaymentDto> payments) {
                view.getActivity().runOnUiThread(() -> {
                    view.hideProgressBar();
                    view.showPayments(payments);
                });
            }
        })).start();
    }

    @Override
    public void onPaymentsClick() {

    }

    @Override
    public void onNewSaleClick() {

    }

    @Override
    public void onSelectItem(PaymentsMVP.PaymentDto item) {
        Bundle params = new Bundle();
        params.putString("name", item.getClient());
        params.putString("address", item.getAddress());

        view.openLocationActivity(params);
    }

    @Override
    public void getUserInfo() {
        new Thread(() -> {
            model.getUserInfo(userInfo -> {
                view.getActivity().runOnUiThread(() -> {
                    view.showUserInfo(userInfo);
                });
            });
        }).start();
    }
}
