package co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.PaymentsInteractor;

public interface PaymentsMVP {

    interface Model {
        void loadPayments(LoadPaymentsCallback callback);

        void getUserInfo(PaymentsInteractor.UserInfoCallback callback);

        interface LoadPaymentsCallback {
            void setPayments(List<PaymentDto> payments);
        }
    }

    interface Presenter {
        void loadPayments();

        void onPaymentsClick();

        void onNewSaleClick();

        void onSelectItem(PaymentDto item);

        void getUserInfo();
    }

    interface View {
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showPayments(List<PaymentDto> payments);

        void openDetailsActivity(Bundle params);

        void showUserInfo(UserInfo userInfo);

        void openLocationActivity(Bundle params);
    }

    class PaymentDto {
        private String client;
        private String address;

        public PaymentDto(String client, String address) {
            this.client = client;
            this.address = address;
        }

        public String getClient() {
            return client;
        }

        public String getAddress() {
            return address;
        }
    }

    public class UserInfo {
        private String name;
        private String email;
        private Boolean enable;

        public UserInfo(String name, String email, Boolean enable) {
            this.name = name;
            this.email = email;
            this.enable = enable;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public Boolean getEnable() {
            return enable;
        }
    }
}
