package co.com.cesardiaz.misiontic.ventasdomiciliog2.model;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.entity.User;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.repository.FirebaseAuthRepository;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.repository.UserRepository;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp.LoginMVP;

public class LoginInteractor implements LoginMVP.Model {

    private final UserRepository userRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;

    public LoginInteractor(Context context) {
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }

    public void createUser(User user, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.createUser(user.getEmail(), user.getPassword(),
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        user.setUid(firebaseAuthRepository.getCurrentUser().getUid());
                        user.setPassword(null);
                        userRepository.createUser(user, new UserRepository.UserCallback<Void>() {
                            @Override
                            public void onSuccess(Void node) {
                                // Enviar confirmacion a presenter
                                callback.onSuccess();
                            }

                            @Override
                            public void onFailure() {
                                // Enviar error a presenter
                                callback.onFailure();
                            }
                        });
                    }

                    @Override
                    public void onFailure() {
                        // Enviar error a presenter
                    }
                });
    }

    @Override
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

        firebaseAuthRepository.validateUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure();
                    }
                });

    }

    @Override
    public void getAllUsers(GetUserCallback<List<LoginMVP.UserInfo>> callback) {
        userRepository.getAllUsers(new UserRepository.UserCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> values) {
                List<LoginMVP.UserInfo> users = new ArrayList<>();
                for (User user : values) {
                    if (user.getEnable() == null || !user.getEnable()) {
                        continue;
                    }

                    users.add(new LoginMVP.UserInfo(user.getName(), user.getEmail()));
                }
                callback.onSuccess(users);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

    @Override
    public boolean hasAuthenticatedUser() {
        return firebaseAuthRepository.isAuthenticated();
    }

    @Override
    public Intent getGoogleIntent() {
        return firebaseAuthRepository.getGoogleSingInIntent();
    }

    @Override
    public void setGoogleData(Intent data, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.setGoogleData(data,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure();
                    }
                });
    }

}
