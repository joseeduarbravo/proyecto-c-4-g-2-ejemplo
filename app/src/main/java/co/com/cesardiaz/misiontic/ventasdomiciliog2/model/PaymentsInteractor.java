package co.com.cesardiaz.misiontic.ventasdomiciliog2.model;

import android.content.Context;

import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.entity.User;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.repository.FirebaseAuthRepository;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.repository.UserRepository;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp.PaymentsMVP;

public class PaymentsInteractor implements PaymentsMVP.Model {

    private List<PaymentsMVP.PaymentDto> payments;
    private UserRepository userRepository;
    private FirebaseAuthRepository firebaseAuthRepository;

    public PaymentsInteractor(Context context) {
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);

        payments = Arrays.asList(
                new PaymentsMVP.PaymentDto("Cesar Diaz", "Calle 1 # 7 - 53, Bogota, Colombia"),
                new PaymentsMVP.PaymentDto("Freddie Valenzuela", "Calle 2 # 15 - 23, Bogota, Colombia"),
                new PaymentsMVP.PaymentDto("Orlando Sedano", "Calle 3 # 2 - 55, Bogota, Colombia"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33, Bogota, Colombia"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56, Bogota, Colombia"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Alexander Carrillo", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Elizabeth Borja", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Frank Polania", "Urb. El Corral Mz A CS 2")
        );

    }

    @Override
    public void loadPayments(LoadPaymentsCallback callback) {
        callback.setPayments(payments);
    }

    @Override
    public void getUserInfo(UserInfoCallback callback) {
        if(firebaseAuthRepository.isAuthenticated()) {
            String uid = firebaseAuthRepository.getCurrentUser().getUid();
            userRepository.getUserByUID(uid, new UserRepository.UserCallback<User>() {
                @Override
                public void onSuccess(User user) {
                    PaymentsMVP.UserInfo userInfo = new PaymentsMVP.UserInfo(user.getName(),
                            user.getEmail(), user.getEnable());
                    callback.onSuccess(userInfo);
                }

                @Override
                public void onFailure() {
                    // No hace nada
                }
            });
        }
    }

    public interface UserInfoCallback {
        void onSuccess(PaymentsMVP.UserInfo userInfo);
    }
}
